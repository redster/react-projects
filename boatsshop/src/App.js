import React from 'react';

import MainPage from './Pages/MainPage/MainPage';

import './App.css';


function App() {
    return (
        <div className="c-header">
            <MainPage/>
        </div>
    );
}

export default App;
