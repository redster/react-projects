import React from 'react';

// Import npm components
import Media from 'react-media';

// Import custom components
import MobileMenu from '../NavBar/Mobile/MobileMenuComponent'
import DesktopMenu from '../NavBar/Desktop/DesktopMenuComponent';

export default function NavBarComponent() {
    return (
        <>
            <Media query="(min-width: 992px)">
                {
                    matches => !matches ? (
                        <>
                            <MobileMenu />
                        </>
                    ):(
                        <>
                            <DesktopMenu />
                        </>
                    ) 
                }
            </Media>
        </>
    );
};