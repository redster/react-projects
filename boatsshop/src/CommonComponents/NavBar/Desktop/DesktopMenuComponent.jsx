import React from 'react';

//import css
import './DesktopMenuComponent.css';

// Import npm components


import logo from '../src/logo.svg';

import image1 from '../src/navbar-elements/1.svg';
import image2 from '../src/navbar-elements/2.svg';
import image3 from '../src/navbar-elements/3.svg';
import image4 from '../src/navbar-elements/4.svg';

const header = () => {
    return (
        <>
            <div className="c-navbar">
                <section className="c-navbar-section"><img src={logo} alt="logo"/></section>
                <section className="c-navbar-section c-navbar-section-slogan">Мы поможем отдыхать{"\n"} качественно</section>
                <section className="c-navbar-section c-navbar-section-contacts"><p>8-931-999-99-99{"\n"} 8-967-678-31-56</p></section>
                <section className="c-navbar-section">
                    <img src={image1} alt="forclients"/>
                    <img src={image2} alt="forclients"/>
                    <img src={image3} alt="forclients"/>
                    <img src={image4} alt="forclients"/>
                </section>       
            </div>
            <div className="c-navbar-buttons">
                <a href="localhost" className="c-navbar-button button active-button">Главная</a>
                <a href="localhost" className="c-navbar-button">Доставка</a>
                <a href="localhost" className="c-navbar-button">Гарантия</a>
                <a href="localhost" className="c-navbar-button">Карта сайта</a>
                <a href="localhost" className="c-navbar-button">Обратная связь</a>
                <a href="localhost" className="c-navbar-button">Отзывы</a>
                <a href="localhost" className="c-navbar-button">Контакты</a>
            </div>
            

            
        </>
        
    )
}

export default header