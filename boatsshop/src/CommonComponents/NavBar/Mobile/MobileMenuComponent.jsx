import React, { useState } from 'react';

// import css
import './MobileMenuComponent.css';

// import custom components
import MobileMenu from '../../ItemMenu/Mobile/MobileItemMenu'

// import img
import purchases from '../src/navbar-elements/3.svg'
import manager from '../src/manager.png'

export default function() {

    const [selected, setSelected] = useState(false);
    
    return (
        <>
            <div className="c-header-nav">
                <div className="c-header-nav-section " onClick={() => {setSelected(!selected);}}>
                    <div className={"c-header-menu-btn" + (selected ? " active" : "")}>
                        <span></span>
                    </div>
                </div>
                <div className="c-header-nav-section c-header-nav-logo"><span className="color">S</span>TEHNIK</div>
                <div className="c-header-nav-section">
                    <img className="c-header-nav-purchases" src={purchases} alt="purchases"></img>
                </div>
            </div>
            <div className={"c-header-nav-menu" + (selected ? " c-header-nav-menu-active" : "")}>
                <div className="c-header-nav-menu-wrapper">
                    <div className="c-header-nav-menu-paragraph">Главная</div>
                    <div className="c-header-nav-menu-paragraph">Доставка</div>
                    <div className="c-header-nav-menu-paragraph">Гарантия</div>
                    <div className="c-header-nav-menu-paragraph">Карта сайта</div>
                    <div className="c-header-nav-menu-paragraph">Обратная связь</div>
                    <div className="c-header-nav-menu-paragraph">Отзывы</div>
                    <div className="c-header-nav-menu-paragraph">Контакты</div>
                </div>
            </div>
            <div className="c-content">
                <h1>Магазин силовой техники</h1>
                <p>Добро пожаловать в наш интернет магазин</p>
                <p>Преимущества нашего сайта:</p>
                <div className="c-content-box">
                    1. Мы стараемся регулярно обновлять каталог силовой техники!{"\n"}
                    2. Стоимость силовой техники в нашем магазине ниже диллерской!{"\n"}
                    3. Заказывая у нас вы получаете товар со скидкой в фирменной упаковке.
                    с пакетом документации и официальной гарантией производителя.{"\n"}
                    4. Доставка техники в регионы через транспортную компанию.{"\n"}
                    5. Доставка по Санкт-Петербургу - курьерской службой.{"\n"}
                </div>
                <div className="c-content-working-time">
                    <p>Наши менеджеры и график работы</p>
                    <p>с 8:00 до 22:00 без выходных</p>
                </div>
                <div className="c-content-managers">
                    <div className="c-content-manager-card">
                        <img className="c-content-managers-photo" src={manager} alt=""/>
                        <p>Имя: Анастасия</p>
                        <p className="c-content-managers-number">+7-891-1123-331</p>
                    </div>                   
                </div>
                <MobileMenu/>




            </div>

        </>
    )   
}

