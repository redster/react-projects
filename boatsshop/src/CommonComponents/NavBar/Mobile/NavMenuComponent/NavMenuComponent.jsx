import React, { useState } from 'react';

//import css
import './NavMenuComponent.css';

const NavMenuComponent = () => {
    const [selected] = useState(false);
    return (
        <div className={"c-header-nav-menu" + (selected ? " c-header-nav-menu-active" : "")}>
            <div className="c-header-nav-menu-wrapper">
                <div className="c-header-nav-menu-paragraph">Главная</div>
                <div className="c-header-nav-menu-paragraph">Доставка</div>
                <div className="c-header-nav-menu-paragraph">Гарантия</div>
                <div className="c-header-nav-menu-paragraph">Карта сайта</div>
                <div className="c-header-nav-menu-paragraph">Обратная связь</div>
                <div className="c-header-nav-menu-paragraph">Отзывы</div>
                <div className="c-header-nav-menu-paragraph">Контакты</div>
            </div>
        </div>
    )
}

export default NavMenuComponent;