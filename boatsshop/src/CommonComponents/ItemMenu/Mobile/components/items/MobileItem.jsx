import React from 'react';

//import css
import './MobileItem.css';

const MobileItem = props => {

    return (
        <div className={`c-mobile-item ${props.className ? this.props.className : ''}`}
            onClick={this.props.btnClick}>                
        </div>
    )
}

export default MobileItem;


{/* <div className="c-mob-item-menu">
<div className="c-first-level-menu" onClick={() => {setSelected(!selected);}}>
    <img  className={"c-first-level-menu-button " + (selected ? "rotate" : "")} src={arrow} alt="arrow"/>
    <p className="c-first-level-menu-paragraph">МОТОРЫ</p>
</div>
</div>
<p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Японские</p>
<p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Корейские</p>
<p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Китайские</p>
<p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Российские</p> */}

