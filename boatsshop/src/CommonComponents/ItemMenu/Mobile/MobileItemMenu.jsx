import React, { useState } from 'react';

//import css
import './MobileItemMenu.css';




import arrow from '../../NavBar/src/arrow.svg'

const MobileItemMenu = () => {

    const [selected, setSelected] = useState(false);

    return(
        <>
            <div className="c-mob-item-menu">
                <div className="c-first-level-menu" onClick={() => {setSelected(!selected);}}>
                    <img  className={"c-first-level-menu-button " + (selected ? "rotate" : "")} src={arrow} alt="arrow"/>
                    <p className="c-first-level-menu-paragraph">МОТОРЫ</p>
                </div>
            </div>
            <p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Японские</p>
            <p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Корейские</p>
            <p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Китайские</p>
            <p className={"c-second-level-menu-paragraph " + (selected ? "transform": "")}>Российские</p>




            <div className="c-mob-item-menu">
                <div className="c-first-level-menu">
                    <img className="c-first-level-menu-button" src={arrow} alt="arrow"/>
                    <p className="c-first-level-menu-paragraph">ЛОДКИ</p>
                </div>
            </div>
            <div className="c-mob-item-menu">
                <div className="c-first-level-menu">
                    <img className="c-first-level-menu-button" src={arrow} alt="arrow"/>
                    <p className="c-first-level-menu-paragraph">МОТОТЕХНИКА</p>
                </div>
            </div>
            <div className="c-mob-item-menu">
                <div className="c-first-level-menu">
                    <img className="c-first-level-menu-button" src={arrow} alt="arrow"/>
                    <p className="c-first-level-menu-paragraph">РОБОТОТЕХНИКА</p>
                </div>
            </div>
            <div className="c-mob-item-menu">
                <div className="c-first-level-menu">
                    <img className="c-first-level-menu-button" src={arrow} alt="arrow"/>
                    <p className="c-first-level-menu-paragraph">ИНСТРУКЦИИ</p>
                </div>
            </div>
        </>
    )
}

export default MobileItemMenu;